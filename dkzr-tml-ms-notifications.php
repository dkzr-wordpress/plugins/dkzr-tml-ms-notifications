<?php
/**
 * Plugin Name: dkzr Theme My Login Multisite Notifications
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/tml-ms-notifications
 * Description: Extend Theme My Login Notifications with Multisite notifications
 * Author: Joost de Keijzer
 * Version: 2.0.1
 * Text Domain: dkzr-tml-ms-notifications
 * Network: true
 *
 * @see https://github.com/johnbillion/wp_mail
 */

class dkzrTmlMsNotifications {
  public function __construct() {
    if ( is_multisite() ) {
      add_action( 'init', [ $this, 'init' ] );
      add_filter( 'tml_notifications_get_default_notifications', [ $this, 'default_notifications' ] );
    }
  }

  public function init() {
    if ( ! function_exists( 'tml_notifications' ) ) {
      return;
    }

    // extra replacements for network_admin_email_change_email hook
    add_filter( 'network_admin_email_change_email', [ $this, 'network_admin_email_change_email_replace_variables' ], 100, 4 );

    // Allow `email_change_confirmation_email` from settings
    add_filter( 'new_user_email_content', [ $this, 'set_filters' ], 100, 2 );

    // filters for mails where wp_mail arguments *are not* filterable in WordPress core
    add_filter( 'wpmu_signup_blog_notification',  [ $this, 'set_filters' ], 100, 7 );
    add_filter( 'wpmu_signup_user_notification',  [ $this, 'set_filters' ], 100, 4 );
    add_filter( 'wpmu_welcome_notification',      [ $this, 'set_filters' ], 100, 5 );
    add_filter( 'wpmu_welcome_user_notification', [ $this, 'set_filters' ], 100, 3 );

    // TODO
    // `invited_user_email` (hook, not cancable)

    // ??? How to handle ¿¿¿
    // `newblog_notify_siteadmin()` (filter with same name for message)
    // `newuser_notify_siteadmin()` (filter with same name for message)
    // `update_network_option_new_admin_email()` (filter new_network_admin_email_content for message)

    $this->register_triggers();

    if ( tml_notifications_is_default_notification_disabled( 'new_site_email' ) ) {
      add_filter( 'send_new_site_email', '__return_false' );
    }

    if ( tml_notifications_is_default_notification_disabled( 'network_admin_email_change_email' ) ) {
      add_filter( 'send_network_admin_email_change_email', '__return_false' );
    }

    if ( tml_notifications_is_default_notification_disabled( 'wpmu_signup_blog_notification' ) ) {
      add_filter( 'wpmu_signup_blog_notification', '__return_false' );
    }

    if ( tml_notifications_is_default_notification_disabled( 'wpmu_signup_user_notification' ) ) {
      add_filter( 'wpmu_signup_user_notification', '__return_false' );
    }

    if ( tml_notifications_is_default_notification_disabled( 'wpmu_welcome_notification' ) ) {
      add_filter( 'wpmu_welcome_notification', '__return_false' );
    }

    if ( tml_notifications_is_default_notification_disabled( 'wpmu_welcome_user_notification' ) ) {
      add_filter( 'wpmu_welcome_user_notification', '__return_false' );
    }
  }

  protected function add_filter_once( $hook, $callback, $priority = 10, $args = 1 ) {
    $singular = function () use ( $hook, $callback, $priority, &$singular ) {
      remove_filter( $hook, $singular, $priority );
      return call_user_func_array( $callback, func_get_args() );
    };

    return add_filter( $hook, $singular, $priority, $args );
  }

  protected function register_triggers() {
    tml_notifications_register_trigger( 'dkzr_tml_after_signup_user', [
      'label' => __( 'New User Signup', 'dkzr-tml-ms-notifications' ),
      'hook' => 'after_signup_user',
      'group' => __( 'Multisite', 'dkzr-tml-ms-notifications' ),
      //'function' => 'dkzr_tml_ms_notifications_user_notification_handler',
    ] );

    tml_notifications_register_trigger( 'dkzr_tml_wpmu_activate_user', [
      'label' => __( 'New User Activated', 'dkzr-tml-ms-notifications' ),
      'hook' => 'wpmu_activate_user',
      'group' => __( 'Multisite', 'dkzr-tml-ms-notifications' ),
      //'function' => 'dkzr_tml_ms_notifications_user_notification_handler',
    ] );

    tml_notifications_register_trigger( 'dkzr_tml_after_signup_site', [
      'label' => __( 'New Site Signup', 'dkzr-tml-ms-notifications' ),
      'hook' => 'after_signup_site',
      'group' => __( 'Multisite', 'dkzr-tml-ms-notifications' ),
      //'function' => 'dkzr_tml_ms_notifications_user_notification_handler',
    ] );

    tml_notifications_register_trigger( 'dkzr_tml_wpmu_activate_blog', [
      'label' => __( 'New Site Activated', 'dkzr-tml-ms-notifications' ),
      'hook' => 'wpmu_activate_blog',
      'group' => __( 'Multisite', 'dkzr-tml-ms-notifications' ),
      //'function' => 'dkzr_tml_ms_notifications_user_notification_handler',
    ] );
  }

  public function default_notifications( $default_notifications ) {
    $saved_notifications = (array) get_site_option( 'tml_notifications_default_notifications', [] );

    // Allow `email_change_confirmation_email` from settings
    if ( isset( $default_notifications['email_change_confirmation_email']['hidden_fields'] ) ) {
      $default_notifications['email_change_confirmation_email']['hidden_fields'] = array_diff(
        $default_notifications['email_change_confirmation_email']['hidden_fields'],
        [ 'from_name', 'from_address' ]
      );
    }

    // Filters for notifications where wp_mail arguments are filtered

    $default_notifications['new_site_email'] = array_merge(
      [
        'title' => __( 'Multisite New Site Admin Notification', 'dkzr-tml-ms-notifications' ),
        'hidden_fields' => [],
      ],
      $saved_notifications['new_site_email'] ?? []
    );

    $default_notifications['network_admin_email_change_email'] = array_merge(
      [
        'title' => __( 'Multisite Network Admin Email Changed Notification', 'dkzr-tml-ms-notifications' ),
        'hidden_fields' => ['recipient'],
      ],
      $saved_notifications['network_admin_email_change_email'] ?? []
    );

    // Filters for notifications where wp_mail arguments *are not* filtered

    $default_notifications['wpmu_signup_blog_notification'] = array_merge(
      [
        'title' => __( 'Multisite Activate Site User Notification', 'dkzr-tml-ms-notifications' ),
        'hidden_fields' => ['recipient', 'format'],
      ],
      $saved_notifications['wpmu_signup_blog_notification'] ?? []
    );

    $default_notifications['wpmu_signup_user_notification'] = array_merge(
      [
        'title' => __( 'Multisite Signup User Notification', 'dkzr-tml-ms-notifications' ),
        'hidden_fields' => ['recipient', 'format'],
      ],
      $saved_notifications['wpmu_signup_user_notification'] ?? []
    );

    $default_notifications['wpmu_welcome_notification'] = array_merge(
      [
        'title' => __( 'Multisite Welcome Site Notification', 'dkzr-tml-ms-notifications' ),
        'hidden_fields' => ['recipient', 'format'],
      ],
      $saved_notifications['wpmu_welcome_notification'] ?? []
    );

    $default_notifications['wpmu_welcome_user_notification'] = array_merge(
      [
        'title' => __( 'Multisite Welcome User Notification', 'dkzr-tml-ms-notifications' ),
        'hidden_fields' => ['recipient', 'format'],
      ],
      $saved_notifications['wpmu_welcome_user_notification'] ?? []
    );

    return $default_notifications;
  }

  public function network_admin_email_change_email_replace_variables( $email_change_email, $old_email, $new_email, $network_id ) {
    $replacements = [
      '%old_email%' => $old_email,
      '%new_email%' => $new_email,
    ];

    $email_change_email['subject'] = tml_notifications_replace_variables( $email_change_email['subject'], null, $replacements );
    $email_change_email['message'] = tml_notifications_replace_variables( $email_change_email['message'], null, $replacements );
  }

  public function set_filters( $return ) {
    if ( ! $return ) {
      // filter disabled on lower priority
      return $return;
    }

    $args = func_get_args();
    $current_filter = current_filter();

    $replacements = [];

    $filters = [
      'wp_mail_from' => [
        'field' => 'from_address',
        'accepted_args' => 1,
      ],
      'wp_mail_from_name' => [
        'field' => 'from_name',
        'accepted_args' => 1,
      ],
    ];

    switch ( $current_filter ) {
      case 'new_user_email_content':
        // only need to set mail_from filters, set current_filter to tml notification_hook
        $current_filter = 'email_change_confirmation_email';

        break;

      case 'wpmu_signup_blog_notification' :
        // $args = $domain, $path, $title, $user_login, $user_email, $key, $meta
        $user = get_user_by( 'login', $args[3] );

        if ( ! is_subdomain_install() || get_current_network_id() != 1 ) {
          $activate_url = network_site_url( "wp-activate.php?key={$args[5]}" );
        } else {
          $activate_url = "http://{$args[0]}{$args[1]}wp-activate.php?key={$args[5]}"; // @todo Use *_url() API.
        }
        $activate_url = esc_url( $activate_url );

        $replacements['%activate_url%'] = $activate_url;

        $filters['wpmu_signup_blog_notification_subject'] = [
          'field' => 'subject',
          'accepted_args' => 7,
        ];

        $filters['wpmu_signup_blog_notification_email'] = [
          'field' => 'message',
          'accepted_args' => 7,
        ];

        break;

      case 'wpmu_signup_user_notification' :
        // $args = $user_login, $user_email, $key, $meta
        $user = get_user_by( 'login', $args[0] );

        $replacements['%activate_url%'] = add_query_arg( [
          'key' => $args[2],
        ], site_url( 'wp-activate.php' ) );

        $filters['wpmu_signup_user_notification_subject'] = [
          'field' => 'subject',
          'accepted_args' => 4,
        ];

        $filters['wpmu_signup_user_notification_email'] = [
          'field' => 'message',
          'accepted_args' => 4,
        ];

        break;

      case 'wpmu_welcome_notification':
        // $args[] = $blog_id, $user_id, $password, $title, $meta
        $user = get_userdata( $args[1] );

        $url = get_blogaddress_by_id( $args[0] );

        $password = $args[2];
        if ( tml_allow_user_passwords() && ! empty( $_POST['user_pass1'] ) ) {
          $password = $_POST['user_pass1'];
        }

        $user_login = $user->user_login;
        if ( tml_is_email_login_type() ) {
          $user_login = $user->user_email;
        }

        $replacements['%username%'] = $user_login;
        $replacements['%password%'] = $password;
        $replacements['%blog_title%'] = $args[3];
        $replacements['%blog_url%'] = $url;

        $filters['update_welcome_subject'] = [
          'field' => 'subject',
          'accepted_args' => 5,
        ];

        $filters['update_welcome_email'] = [
          'field' => 'message',
          'accepted_args' => 5,
        ];

        break;

      case 'wpmu_welcome_user_notification':
        // $args[] = $user_id, $password, $meta
        $user = get_userdata( $args[0] );

        $password = $args[1];
        if ( tml_allow_user_passwords() && ! empty( $_POST['user_pass1'] ) ) {
          $password = $_POST['user_pass1'];
        }

        $user_login = $user->user_login;
        if ( tml_is_email_login_type() ) {
          $user_login = $user->user_email;
        }

        $replacements['%username%'] = $user_login;
        $replacements['%password%'] = $password;

        $filters['update_welcome_user_subject'] = [
          'field' => 'subject',
          'accepted_args' => 3,
        ];

        $filters['update_welcome_user_email'] = [
          'field' => 'message',
          'accepted_args' => 3,
        ];

        break;

      default:
        return $return;
        break;
    }

    foreach( $filters as $hook => $settings ) {
      $field = $settings['field'];
      $callback = function( $content ) use( $field, $current_filter, $user, $replacements ) { return $this->set_content( $content, $field, $current_filter, $user, $replacements, func_get_args() ); };

      $this->add_filter_once( $hook, $callback, 100, $settings['accepted_args'] );
    }

    return $return;
  }

  public function set_content( $content, $field, $notification_hook, $user, $replacements, $args ) {
    if ( ! $notification = tml_notifications_get_default_notification( $notification_hook ) ) {
      return $content;
    }

    if ( empty( $notification[ $field ] ) ) {
      return $content;
    } else {
      $content = $notification[ $field ];
    }

    if ( in_array( $field, [ 'subject', 'message' ] ) ) {
      $content = tml_notifications_replace_variables( $content, $user, $replacements );
    }

    return $content;
  }
}
$dkzr_tml_ms_notifications = new dkzrTmlMsNotifications();
